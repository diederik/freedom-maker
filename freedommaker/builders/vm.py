# SPDX-License-Identifier: GPL-3.0-or-later
"""
Base worker class to build Virtual Machine images.
"""

import os

from .. import utils
from .amd_intel import AMDIntelImageBuilder


class VMImageBuilder(AMDIntelImageBuilder):
    """Base image builder for all virtual machine targets."""
    vm_image_extension: str | None = None

    @property
    def image_size(self):
        """Return the additional size for the image for usage."""
        from ..application import IMAGE_SIZE
        if self.arguments.image_size == IMAGE_SIZE:
            return utils.add_disk_sizes(IMAGE_SIZE, '12G')

        return self.arguments.image_size

    def build(self):
        """Run the image building process."""
        vm_file = self._replace_extension(self.image_file,
                                          self.vm_image_extension)
        vm_archive_file = vm_file + '.xz'

        self.make_image()
        self.create_vm_file(self.image_file, vm_file)
        os.remove(self.image_file)
        self.compress(vm_archive_file, vm_file)

        self.sign(vm_archive_file)

    def create_vm_file(self, image_file, vm_file):
        """Create a VM image from image file."""
        raise Exception('Not reached')
